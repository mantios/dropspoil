L2 Shrine Drop and Spoil Calculator
========================

L2 Shrine Drop and Spoil Calculator is a web application written in Symfony 3 framework. It's purpose is to give players information about NPC drop, spoil and position, which are generated directly from server data.

Generating Drop, Spoil and Position Data
--------------

L2 Shrine Drop and Spoil Calculator comes with a command, that fills up database with necessary data (including npc positions).

Before running the command:

  * Make sure to generate database schema using doctrine command: `php bin/console doctrine:schema:create`.

  * Prepare `ItemName-e.txt`, `NpcName-e.txt`, `Armorgrp.txt`, `EtcItemgrp.txt`, `Weapongrp.txt` by decoding client files. 

  * Generate `dropspoil.txt` using MyExt64 Extender - turn on `Dump` flag to `true` in `MyExt64.ini`.

  * Generate `npcpositions.json` using python script below (script doesn't take minions into account, Emca will add that functionality later).

  * Make sure that memory limit for PHP is at least 128M.

  * Run command `php bin/console dropspoil:generate` with all necessary options (`php bin/console dropspoil:generate -h`) and let it generate database data.

  * [Deploy the Symfony 3 application](https://symfony.com/doc/current/deployment.html) as usual.

Python script for generating npcpositions.json
--------------
```python
#!/usr/bin/env python

import re
from json import dumps
from random import randint

def isInside(x, y, poly):
    n = len(poly)
    inside = False
    p1x, p1y = poly[0]
    for i in range(n + 1):
        p2x, p2y = poly[i % n]
        if y > min(p1y, p2y):
            if y <= max(p1y, p2y):
                if x <= max(p1x, p2x):
                    if p1y != p2y:
                        xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                    if p1x == p2x or x <= xinters:
                        inside = not inside
        p1x, p1y = p2x, p2y
    return inside

data = open("npc_pch.txt", "rb").read()

regex = re.compile("\[(.*?)\]\s+=\s+([0-9]+)")

npcs = {}

for match in re.findall(regex, data):
    npcs[match[0]] = int(match[1])

territoryRegex = re.compile("territory_(?:ex_)?begin.*?{{(.*?)}}.*?territory_(?:ex_)?end")
npcRegex = re.compile("npc_(?:ex_)?begin.*?\[(.*?)\].*?pos={(.*?)}.*?npc_(?:ex_)?end")
npcRegexMulti = re.compile("npc_(?:ex_)?begin.*?\[(.*?)\].*?pos={{(.*?)}}.*?npc_(?:ex_)?end")
npcRegexAnywhere = re.compile("npc_(?:ex_)?begin.*?\[(.*?)\].*?pos=anywhere.*?total=([0-9]+).*?npc_(?:ex_)?end")

npcpos = {}

for line in open("npcpos.txt", "rb"):
    line = line.strip("\n").strip("\r")
    if line.strip().startswith("//"): continue
    match = re.match(territoryRegex, line)
    if match:
        poly = [tuple([int(j) for j in i.split(";")[:2]]) for i in match.groups()[0].split("};{")]
        minx = poly[0][0]
        miny = poly[0][1]
        maxx = poly[0][0]
        maxy = poly[0][1]
        for point in poly[1:]:
            minx = min(minx, point[0])
            maxx = max(maxx, point[0])
            miny = min(miny, point[1])
            maxy = max(maxy, point[1])
        continue
    match = re.match(npcRegexMulti, line)
    if match:
        match = match.groups()
        points = [tuple([int(j) for j in i.split(";")[:2]]) for i in match[1].split("};{")]
        for point in points:
            npcpos.setdefault(npcs.get(match[0]) - 1000000, []).append({"x" : point[0], "y" : point[1]})
        continue
    match = re.match(npcRegex, line)
    if match:
        match = match.groups()
        point = tuple([int(i) for i in match[1].split(";")[:2]])
        npcpos.setdefault(npcs.get(match[0]) - 1000000, []).append({"x" : point[0], "y" : point[1]})
        continue
    match = re.match(npcRegexAnywhere, line)
    if match:
        match = match.groups()
        match = [match[0]] + [int(i) for i in match[1:]]
        for i in xrange(match[1]):
            tries = 0
            ok = True
            while True:
                x = randint(minx, maxx)
                y = randint(miny, maxy)
                if isInside(x, y, poly):
                    break
                tries += 1
                if tries > 100:
                    print "Giving up", match[0]
                    ok = False
                    break
            if ok:
                npcpos.setdefault(npcs.get(match[0]) - 1000000, []).append({"x" : x, "y" : y})
        continue
    if line.find("territory_begin") != -1:
        print line
        raise Exception("TERRITORY NOT MATCHED")
    if line.find("npc_begin") != -1:
        print line
        raise Exception("NPC NOT MATCHED")

output = open("npcpositions.json", "wb")
output.write(dumps(npcpos, indent = 4))
output.close()
```
