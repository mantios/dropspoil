<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class ItemRepository extends EntityRepository
{

    public function findByName($name)
    {
        $qb = $this->createQueryBuilder('i');
        if (!empty($name)) {
            $qb = $qb->where(
                $qb->expr()->like('i.name', '?1')
            )->setParameter(1, '%'.$name.'%');
        }

        return $qb->getQuery()->getResult();
    }

}