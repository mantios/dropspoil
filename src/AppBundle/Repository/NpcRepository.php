<?php

namespace AppBundle\Repository;


use Doctrine\ORM\EntityRepository;

class NpcRepository extends EntityRepository
{

    public function findByName($name)
    {
        $qb = $this->createQueryBuilder('n');
        if (!empty($name)) {
            $qb = $qb->where(
                $qb->expr()->like('n.name', '?1')
            )->setParameter(1, '%'.$name.'%');
        }

        return $qb->getQuery()->getResult();
    }

}