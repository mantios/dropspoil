<?php

namespace AppBundle\Command;

use AppBundle\Entity\Drop;
use AppBundle\Entity\Item;
use AppBundle\Entity\Npc;
use AppBundle\Entity\NpcPosition;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class GenerateDropSpoilCommand
 * @package AppBundle\Command
 */
class GenerateDropSpoilCommand extends ContainerAwareCommand
{
    use LockableTrait;

    const FILES = [
        ['dropspoil', 'Path to dropspoil file.', 'dropspoil.txt'],
        ['itemname', 'Path to decoded ItemName-e.dat file.', 'ItemName-e.txt'],
        ['npcname', 'Path to decoded NpcName-e.dat file.', 'NpcName-e.txt'],
        ['armorgrp', 'Path to decoded Armorgrp.dat file.', 'Armorgrp.txt'],
        ['etcitemgrp', 'Path to decoded EtcItemgrp.dat file.', 'EtcItemgrp.txt'],
        ['weapongrp', 'Path to decoded Weapongrp.dat file.', 'Weapongrp.txt'],
        ['npcpositions', 'Path to npcpositions.json file', 'npcpositions.json'],
    ];

    const BATCH_SIZE = 20;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('dropspoil:generate')
            ->setDescription('Generates dropspoil and NPC positions from data files.');
        foreach (self::FILES as $file) {
            $this->addOption($file[0], null, InputOption::VALUE_REQUIRED, $file[1], $file[2]);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // disable logger to significantly reduce memory consumption by Doctrine
        $this->getContainer()->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('Generator tool for dropspoil and NPC positions.');

        if (!$this->lock()) {
            $this->io->error('Command is already running in another thread.');

            return;
        }

        $this->io->section('Files verification');

        // verify that all required files exist
        $errors = [];
        foreach (self::FILES as $file) {
            $filename = $input->getOption($file[0]);
            if (!file_exists(realpath($filename))) {
                $errors[] = sprintf('File %s not found. Please verify that the file is present.', $filename);
            }
        }

        if (!empty($errors)) {
            foreach ($errors as $error) {
                $this->io->error($error);
            }

            return;
        }

        $this->io->success('All files found on the file system.');

        // create items
        $this->io->section('Items');

        $this->createItems(
            $input->getOption('itemname'),
            $input->getOption('armorgrp'),
            $input->getOption('etcitemgrp'),
            $input->getOption('weapongrp')
        );

        // create npcs
        $this->io->section('NPCs');

        $this->createNpcs(
            $input->getOption('npcname'),
            $input->getOption('npcpositions')
        );

        // create drops
        $this->io->section('Drops');

        $this->createDrops(
            $input->getOption('dropspoil')
        );

        $this->io->success('Done.');
    }

    private function createItems($itemnamePath, $armorgrpPath, $etcitemgrpPath, $weapongrpPath)
    {
        $items = [];

        $this->processFile(
            $itemnamePath,
            true,
            function ($line) use (&$items) {
                $pieces = explode("\t", $line);
                $item = new Item();
                $item->setId(intval(trim($pieces[0])));
                $item->setName(trim($pieces[1]));
                $item->setAddName(trim($pieces[2]));
                $item->setDescription(str_replace(['a,', '\0'], '', trim($pieces[3])));
                $items[$item->getId()] = $item;
            }
        );

        // set icons
        $this->io->writeln('Parsing grp files to extract icons.');

        foreach ([$armorgrpPath, $etcitemgrpPath, $weapongrpPath] as $path) {
            $this->processFile(
                $path,
                true,
                function ($line) use ($items) {
                    $pieces = explode("\t", $line);
                    $item = $items[$pieces[1]];
                    $icon = trim($pieces[22]);
                    if (!empty($icon)) {
                        $item->setIcon(str_replace('icon.', '', $icon).'.png');
                    }
                }
            );
        }

        $this->persist($items);

        $this->io->success('All items successfully processed');
    }

    private function createNpcs($npcnamePath, $npcpositionsPath)
    {
        $npcs = [];

        $this->processFile(
            $npcnamePath,
            true,
            function ($line) use (&$npcs) {
                $pieces = explode("\t", $line);
                $npc = new Npc();
                $npc->setId(intval(trim($pieces[0])));
                $npc->setName(str_replace(['a,', '\0'], '', trim($pieces[1])));
                $npc->setDescription(str_replace(['a,', '\0'], '', trim($pieces[2])));
                $npcs[$npc->getId()] = $npc;
            }
        );

        $this->io->writeln('Processing NPC positions');

        // load positions
        $this->io->writeln(sprintf('Loading %s into memory.', $npcpositionsPath));
        $json = json_decode(file_get_contents($npcpositionsPath), true);
        $itemCount = count($json);
        $this->io->progressStart($itemCount);
        foreach ($json as $id => $positions) {
            $npc = $npcs[$id];
            foreach ($positions as $position) {
                $npcPosition = new NpcPosition();
                $npcPosition->setX($position['x']);
                $npcPosition->setY($position['y']);
                $npcPosition->setNpc($npc);
                $npc->addPosition($npcPosition);
            }
            $this->io->progressAdvance();
        }
        $this->io->progressFinish();
        $this->io->writeln(sprintf('Processed %s positions.', $itemCount));
        $this->io->newLine();

        $this->persist($npcs);

        $this->io->success('All NPCs successfully processed');
    }

    private function createDrops($dropspoilPath)
    {
        $npcRepository = $this->getContainer()->get('doctrine')->getRepository(Npc::class);
        $itemRepository = $this->getContainer()->get('doctrine')->getRepository(Item::class);

        $this->processFile(
            $dropspoilPath,
            false,
            function ($line) use ($npcRepository, $itemRepository) {
                $pieces = explode("\t", trim($line));
                $npc = $npcRepository->find($pieces[0]);
                $item = $itemRepository->find($pieces[2]);
                if ($npc != null && $item != null) {
                    $npcs[] = $npc;
                    $items[] = $item;
                    $drop = new Drop();
                    $drop->setNpc($npc);
                    $drop->setItem($item);
                    $drop->setType($pieces[1]);
                    $drop->setMin($pieces[3]);
                    $drop->setMax($pieces[4]);
                    $drop->setChance($pieces[5]);
                    $drop->setGroup($pieces[6]);
                    $drop->setGroupChance($pieces[7]);
                    $npc->addDrop($drop);
                    $item->addDrop($drop);
                    // flush right away to keep memory consumption low
                    $this->em->flush();
                    $this->em->clear();
                }
            }
        );

        $this->io->newLine();

        $this->io->success('All drops successfully processed');
    }

    private function processFile($filePath, $skipHeader, $callback)
    {
        $this->io->writeln(sprintf('Processing %s.', $filePath));

        $handle = fopen($filePath, 'r');
        $lineCount = 0;
        if ($handle) {
            $this->io->progressStart();
            while (($line = fgets($handle)) !== false) {
                if ($skipHeader) {
                    $skipHeader = false;
                    continue;
                }
                $line = trim($line);
                $callback($line);
                $lineCount++;
                $this->io->progressAdvance();
            }
            $this->io->progressFinish();
            $this->io->writeln(sprintf('Processed %s lines.', $lineCount));
            $this->io->newLine();
            fclose($handle);
        } else {
            $this->io->error(sprintf('Unable to open %s for reading.', $filePath));
        }
    }

    private function persist($objects)
    {
        $this->io->writeln('Persisting entities into database.');

        $i = 0;
        $itemCount = count($objects);
        $this->io->progressStart($itemCount);
        foreach ($objects as $object) {
            $this->em->persist($object);
            $this->io->progressAdvance();
            $i++;
            if (($i % self::BATCH_SIZE) == 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }
        $this->em->flush();
        $this->em->clear();
        $this->io->progressFinish();

        $this->io->writeln(sprintf('%s entities saved', $itemCount));
        $this->io->newLine();
    }
}
