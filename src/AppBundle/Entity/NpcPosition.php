<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="npc_positions")
 * @package AppBundle\Entity
 */
class NpcPosition
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int $id
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Npc", inversedBy="positions")
     * @var Npc $npc
     */
    private $npc;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int $x;
     */
    private $x;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int $y;
     */
    private $y;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Npc
     */
    public function getNpc()
    {
        return $this->npc;
    }

    /**
     * @param Npc $npc
     */
    public function setNpc($npc)
    {
        $this->npc = $npc;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }
}