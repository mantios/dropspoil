<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DropRepository")
 * @ORM\Table(name="drops")
 * @package AppBundle\Entity
 */
class Drop
{
    const SPOIL = 144;
    const DROP = 217;
    const HERB = 250;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int $id
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Npc", inversedBy="drops")
     * @var Npc $npc
     */
    private $npc;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Item", inversedBy="drops")
     * @var Item $item
     */
    private $item;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int $type
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int $min
     */
    private $min;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @var int $max
     */
    private $max;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float $chance
     */
    private $chance;

    /**
     * @ORM\Column(name="`group`", type="integer", nullable=false)
     * @var int $group
     */
    private $group;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @var float $groupChance
     */
    private $groupChance;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Npc
     */
    public function getNpc()
    {
        return $this->npc;
    }

    /**
     * @param Npc $npc
     */
    public function setNpc($npc)
    {
        $this->npc = $npc;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param int $min
     */
    public function setMin($min)
    {
        $this->min = $min;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * @param int $max
     */
    public function setMax($max)
    {
        $this->max = $max;
    }

    /**
     * @return float
     */
    public function getChance()
    {
        return $this->chance;
    }

    /**
     * @param float $chance
     */
    public function setChance($chance)
    {
        $this->chance = $chance;
    }

    /**
     * @return int
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param int $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return float
     */
    public function getGroupChance()
    {
        return $this->groupChance;
    }

    /**
     * @param float $groupChance
     */
    public function setGroupChance($groupChance)
    {
        $this->groupChance = $groupChance;
    }
}