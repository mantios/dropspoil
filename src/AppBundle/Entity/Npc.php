<?php

namespace AppBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NpcRepository")
 * @ORM\Table(name="npcs", indexes={@Index(name="npc_idx", columns={"name"})})
 * @package AppBundle\Entity
 */
class Npc
{

    /**
     * @ORM\Column(type="integer", unique=true, nullable=false)
     * @ORM\Id()
     * @var int $id
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @var string $name
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string $description
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NpcPosition", mappedBy="npc", cascade={"persist", "remove"})
     * @var ArrayCollection $positions
     */
    private $positions;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Drop", mappedBy="npc", cascade={"persist", "remove"})
     * @var ArrayCollection $drops
     */
    private $drops;

    /**
     * Npc constructor.
     */
    public function __construct()
    {
        $this->positions = new ArrayCollection();
        $this->drops = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param ArrayCollection $positions
     */
    public function setPositions($positions)
    {
        $this->positions = $positions;
    }

    /**
     * @param NpcPosition $position
     */
    public function addPosition(NpcPosition $position)
    {
        $this->positions->add($position);
    }

    /**
     * @return ArrayCollection
     */
    public function getDrops()
    {
        return $this->drops;
    }

    /**
     * @param ArrayCollection $drops
     */
    public function setDrops($drops)
    {
        $this->drops = $drops;
    }

    /**
     * @param Drop $drop
     */
    public function addDrop($drop)
    {
        $this->drops->add($drop);
    }
}